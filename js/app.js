$(document).ready(function () {
  $(".item-show").click(function () {
    $(this).next(".mostrar-carrer").toggleClass("item-activo");
    $(this).toggleClass("show-red")
  });


  $('.container-image-desktop').click(function () {
    var logro = $(this).data("logro")

    $('.image-logro-desktop').addClass("hidden")
    $('#' + logro).removeClass("hidden")

    $('.container-image-desktop').removeClass("logro-bg-red")
    $(this).addClass("logro-bg-red")

    $('.arrow-right').addClass("hidden")
    $(this).children('.arrow-right').removeClass("hidden")
    console.log('tab');
  })

  $('.container-schedule-mobile').click(function () {
    $(this).children('.show-schedule-mobile').toggleClass("hidden")
    $(this).children('.hidden-schedule-mobile').toggleClass("hidden")
    $(this).next('.schedule-mobile').toggleClass("hidden-xs hidden-sm")

    $('.container-schedule-mobile').removeClass("schedule-bg-red")
    $(this).addClass("schedule-bg-red")
  })
})